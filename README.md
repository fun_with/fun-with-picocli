# 💻  CLI to manage your computer

In order to discover [picocli](https://picocli.info/) features, I started to created a little CLI based on `Kotlin` to manage stuff on your computer.

Currently covered : 
- List projects in a given directory to know if they are under `git` or not. And if so, where `🦊 Gitlab` or `🐙 Github`


To come :
- Update all projects : `git`
- Find which build tool(s) : `maven`, `gradle`, `yarn`, `npm`...
- Clean projects
- and probably more to come 
