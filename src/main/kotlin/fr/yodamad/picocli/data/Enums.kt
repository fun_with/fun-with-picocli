package fr.yodamad.picocli.data

enum class GIT_REMOTE { empty, none, gitlab, github }
