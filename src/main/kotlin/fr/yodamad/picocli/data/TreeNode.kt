package fr.yodamad.picocli.data

import com.andreapivetta.kolor.green
import com.andreapivetta.kolor.red
import com.andreapivetta.kolor.yellow
import fr.yodamad.picocli.data.GIT_REMOTE.empty
import fr.yodamad.picocli.data.GIT_REMOTE.gitlab

class TreeNode<T>(value: T) {
    var value: T = value
    var parent: TreeNode<T>? = null
    var children: MutableList<TreeNode<T>> = mutableListOf()

    var git = empty
    var depth = 0

    fun addChild(node: TreeNode<T>) {
        children.add(node)
        node.parent = this
    }

    override fun toString(): String {
        var string = "  ".repeat(depth)
        if (parent != null) {
            if (git != empty) {
                string += "|_ ${value.toString().removePrefix(parent!!.value.toString()).green()} is under git"
                if (git == gitlab) string += " (hosted on 🦊 Gitlab)"
                else string += " (not hosted yet ⚠️ )"
            } else {
                string += "|_ ${value.toString().removePrefix(parent!!.value.toString()).yellow()}"
            }
        } else {
            string = "${value.toString().red()}"
        }
        return string
    }
}
