package fr.yodamad.picocli

import fr.yodamad.picocli.data.GIT_REMOTE.gitlab
import fr.yodamad.picocli.data.GIT_REMOTE.none
import fr.yodamad.picocli.data.TreeNode
import fr.yodamad.picocli.functions.isGitDirectory
import fr.yodamad.picocli.functions.isHostedOnGitlab
import picocli.CommandLine.Command
import picocli.CommandLine.Option
import java.io.File
import java.util.concurrent.Callable

@Command(name = "git")
class Git : Callable<Int> {

    @Option(names = ["-l"], description = ["List of projects managed by GIT"])
    var list = false

    lateinit var tree: TreeNode<File>

    override fun call(): Int {
        val home = ""
        tree = TreeNode(File(home))

        listProjects(tree)
        printStructure(tree)
        return 0
    }

    private fun printStructure(node: TreeNode<File>) {
        println(node)
        if (node.children.isNotEmpty()) {
            node.children.map { printStructure(it) }
        }
    }

    private fun listProjects(currentNode: TreeNode<File>) {
        if (currentNode.value.list().isNotEmpty()) {
            currentNode.value.listFiles().forEach { l ->
                run {
                    val newNode = TreeNode<File>(l)
                    newNode.depth = currentNode.depth+1
                    if (l.isDirectory) {
                        currentNode.addChild(newNode)
                        if (l.isGitDirectory()) {
                            if (l.isHostedOnGitlab()) newNode.git = gitlab
                            else newNode.git = none
                        } else listProjects(newNode)
                    }
                }
            }
        }
    }
}
