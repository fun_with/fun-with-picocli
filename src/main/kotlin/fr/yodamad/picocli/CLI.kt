package fr.yodamad.picocli

import picocli.CommandLine
import picocli.CommandLine.Command
import java.util.concurrent.Callable
import kotlin.system.exitProcess

@Command(name="cli", subcommands = [Git::class])
class CLI: Callable<Int> {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) : Unit = exitProcess(CommandLine(CLI()).execute(*args))
    }

    override fun call(): Int = 23
}
