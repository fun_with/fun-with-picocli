package fr.yodamad.picocli.functions

import fr.yodamad.picocli.data.GIT_REMOTE
import org.eclipse.jgit.api.Git
import java.io.File

fun File.isGitDirectory() = this.isDirectory && this.list().contains(".git")

fun File.isHostedOnGitlab(): Boolean {
    val remotes = Git.open(this).remoteList().call()
    return if (remotes.isNotEmpty()) remotes[0].urIs.any { it.host.contains(GIT_REMOTE.gitlab.name) }
    else false
}
